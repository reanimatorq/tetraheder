#include "FastLED.h"

#define LED_PIN 6
#define LEDCount 273
#define CHIPSET WS2812
#define COLOR_ORDER GRB
#define NUM_LEDS 273
#define BRIGHTNESS 120

CRGB leds[NUM_LEDS];
int rechts[NUM_LEDS/3]={
  0,3,4,9,10,11,18,19,20,21,30,31,32,33,34,45,46,47,48,49,50,63,64,65,66,67,68,69,84,85,86,87,88,89,90,91,108,109,110,111,112,113,114,115,116,135,136,137,138,139,140,141,142,143,144,165,166,167,168,169,170,171,172,173,174,175,198,199,200,201,202,203,204,205,206,207,208,209,234,235,236,237,238,239,240,241,242,243,244,245,246};
int rechtsMirror[]={
  0,4,3,11,10,9,21,20,19,18,34,33,32,31,30,50,49,48,47,46,45,69,68,67,66,65,64,63,91,90,89,88,87,86,85,84,116,115,114,113,112,111,110,109,108,144,143,142,141,140,139,138,137,136,135,175,174,173,172,171,170,169,168,167,166,165,209,208,207,206,205,204,203,202,201,200,199,198,246,245,244,243,242,241,240,239,238,237,236,235,234};
int links[NUM_LEDS/3]={
  1,5,6,12,13,14,22,23,24,25,35,36,37,38,39,51,52,53,54,55,56,70,71,72,73,74,75,76,92,93,94,95,96,97,98,99,117,118,119,120,121,122,123,124,125,145,146,147,148,149,150,151,152,153,154,176,177,178,179,180,181,182,183,184,185,186,210,211,212,213,214,215,216,217,218,219,220,221,247,248,249,250,251,252,253,254,255,256,257,258,259};
int hinten[NUM_LEDS/3]={
  2,7,8,15,16,17,26,27,28,29,40,41,42,43,44,57,58,59,60,61,62,77,78,79,80,81,82,83,100,101,102,103,104,105,106,107,126,127,128,129,130,131,132,133,134,155,156,157,158,159,160,161,162,163,164,187,188,189,190,191,192,193,194,195,196,197,222,223,224,225,226,227,228,229,230,231,232,233,260,261,262,263,264,265,266,267,268,269,270,271,272};

int letters[][27]={
{207,173,142,114,89,67,48,32,47,65,86,110,137,167,200,113,112,111},//0=A
{},//1=B
{},//2=C
{},//3=D
{204,203,140,88,87,48,20,19,18,202,19},//4=E
{},//5=F
{},//6=G
{21,49,89,141,205,18,46,86,138,202,88,87},//7=H
{169,171,170,112,66,32,31,33},//8=I
{},//9=J
{},//10=K
{},//11=L
{},//12=M
{},//13=N
{},//14=O
{170,112,66,32,31,33,30,45,64,86,87,33},//15=P
{},//16=Q
{},//17=R
{},//18=S
{},//19=T
{},//20=U
{},//21=V
{},//22=W
{},//23=X
{240,170,112,66,48,33,21,47,31,18},//24=Y
{},//25=Z
{68,49,20,33,32,66,112,170}//26=0
};

int letterLength[27]={
18,0,0,0,11,0,0,12,8,0,0,0,0,0,0,12,0,0,0,0,0,0,0,0,10,0,8};



void copyToOther(){
  for(int i=0;i<NUM_LEDS/3;++i){
    leds[links[i]]=leds[rechts[i]];
    leds[hinten[i]]=leds[rechts[i]];
  }
}

void setup() {
  Serial.begin(9600);
  randomSeed(analogRead(0)); 
  delay(3000);
  FastLED.addLeds<CHIPSET, LED_PIN, COLOR_ORDER>(leds, NUM_LEDS);
  FastLED.setBrightness(BRIGHTNESS);
  randomSeed(analogRead(0));
  FastLED.clear();
  FastLED.show(); 
}

void printLetter(int letter){
  FastLED.clear();
  for(int i=0;i<letterLength[letter];++i){
    leds[letters[letter][i]]=CHSV(120,120,120);
  }
  copyToOther();
  FastLED.show();
  delay(500);
}

void printCodedWord(int string[],int length){
  for(int i=0;i<length;++i){
    printLetter(string[i]);
  }
}

void rainbowFade(){
  for(int j=0;j<359;++j){
    for(int i=0;i<NUM_LEDS/3;++i){
      leds[rechts[i]]=CHSV(j, 255, BRIGHTNESS);
      leds[links[i]]=CHSV(j, 255, BRIGHTNESS);
      leds[hinten[i]]=CHSV(j, 255, BRIGHTNESS);
    }
    FastLED.show();
    delay(10);
  }
}

void triangle(int counts){
  for(int count=0;count<counts;++count){
    FastLED.clear();
    int p=0;
    for(int a=0;a<5;++a){
      FastLED.show();
      for(int i=p,row=1;row<=13;i=i+(row++*3)+p){
        for(int j=0+a,count=i+a;j<row-a;++j){
          if(count==i+a||count==i+row-1-a||row==13-a){
            if(row<14-a){
              leds[count]= CHSV(i, 255, BRIGHTNESS);
            }
          }
          count++;
        }
      }
      copyToOther();
      FastLED.show();
      delay(200);	              
    }
  }
}

void triangle2(int counts){
  for(int count=0;count<counts;++count){
    FastLED.clear();
    int p=0;
    for(int a=0;a<5;++a){
      FastLED.show();
      for(int i=p,row=1;row<=13;i=i+(row++*3)+p){
        for(int j=0+a,count=i+a;j<row-a;++j){
          if(count==i+a||count==i+row-1-a||row==13-a){
            if(row<14-a){
              leds[count]= CHSV(273-i, 255, BRIGHTNESS);
            }
          }
          count++;
        }
      }
      copyToOther();
      FastLED.show();
      delay(150);	              
    }
    p=0;
    for(int a=0;a<5;++a){
      FastLED.show();
      for(int i=p,row=1;row<=13;i=i+(row++*3)+p){
        for(int j=0+a,count=i+a;j<row-a;++j){
          if(count==i+a||count==i+row-1-a||row==13-a){
            if(row<14-a){
              leds[count]= CHSV(0, 0, 0);
            }
          }
          count++;
        }
      }
      copyToOther();
      FastLED.show();
      delay(150);	              
    }
  }
}

void around(int turns){
  FastLED.clear();
  FastLED.show();
  for(int count=0;count<turns;++count){
    int length=100;
    for(int i=0;i<NUM_LEDS+length;++i){
      if(i<NUM_LEDS)
        leds[i]=CHSV(i, 255, BRIGHTNESS);
      FastLED.show();
      delay(5);
      if(i>=length)
        leds[i-length]=CHSV(0, 0, 0);
    }
  }
}

void delRows(int rowsToDel){
  int count=0;
  for(int rowlength=3;rowlength<3*rowsToDel+1;rowlength+=3){
    for(int i=0;i<rowlength;++i){
      leds[count++]=CHSV(0, 0, 0);
    }
  }
}

void rows(int turns, int rowsToPrint){
  FastLED.clear();
  FastLED.show();
  for(int j=0;j<turns;++j){
    int count=0;
    int row=1;
    for(int rowlength=3;rowlength<3*(13+rowsToPrint)+1;rowlength+=3){
      if(rowlength>3*rowsToPrint+1){
        delRows(row-rowsToPrint);
      }
      for(int i=0;i<rowlength;++i){
        if(rowlength<3*13+1){
          leds[count]=CHSV(count, 255, BRIGHTNESS);
        }
        ++count;
      }
      if(rowlength<3*13+1){
        FastLED.show();
        delay(100);
      }
      ++row;
    }
  }
}

void filledRectAround(int turns){
  FastLED.clear();
  FastLED.show();
  for(int turn=0;turn<turns;++turn){
    for(int i=0;i<NUM_LEDS/3;++i){
      leds[rechts[i]]=CHSV(i, 255, BRIGHTNESS);
    }
    FastLED.show();
    delay(100);
    FastLED.clear();
    for(int i=0;i<NUM_LEDS/3;++i){
      leds[links[i]]=CHSV(i, 255, BRIGHTNESS);
    }
    FastLED.show();
    delay(100);
    FastLED.clear();
    for(int i=0;i<NUM_LEDS/3;++i){
      leds[hinten[i]]=CHSV(i, 255, BRIGHTNESS);
    }
    FastLED.show();
    delay(100);
    FastLED.clear();
  }
}

void randomPoints(int pointCount, int turns){
  for(int turn=0;turn<turns;++turn){
    FastLED.clear();
    for(int i=0;i<pointCount;++i){
      leds[rechts[random(NUM_LEDS/3)]]=CHSV(random(255), 255, BRIGHTNESS);
    }
    copyToOther();
    FastLED.show();
    delay(5);
  }
}

#define dist(a, b, c, d) sqrt(double((a - c) * (a - c) + (b - d) * (b - d)))

void plasma(int loops){
  double time=100000.25456;
  for(int i=0;i<loops;++i){
    int counter=0;
    time = time+(2.612554+(millis()%2));
    for(int x = 0,row = 1; x < 13; x++,row++){
      for(int y = 0; y < 13; y++)
      {
        if(y<row){
          double value = sin(dist(x + time, y, 128.0, 128.0) / 8.0)
            + sin(dist(x, y, 64.0, 64.0) / 8.0)
              + sin(dist(x, y + time / 7, 192.0, 64) / 7.0)
                + sin(dist(x, y, 192.0, 100.0) / 8.0);
          int color = int((4 + value)) * 32;

          CRGB temp = CRGB(color, color * 2, 255 - color);
          leds[(links[counter])]=temp;
          leds[rechtsMirror[counter]]=temp;
          leds[(hinten[counter])]=temp;
          counter++;
        }
      }
    } 
    FastLED.show();
    delay(1); 
  }
}

void randomAnimation(){
  int temp = random(1,9);
  
  switch (temp) {
    case 1:
      randomPoints(random(20,40),random(50,150));
      break;
    case 2:
      rows(random(1,4),random(1,3));
      break;
      case 3:
      filledRectAround(random(10,20));
      break;
       case 4:
      rainbowFade();
      break;
       case 5:
      triangle(random(1,5));
      break;
       case 6:
      around(random(1,3));
      break;
      case 7:
      triangle2(random(1,5));
      break;
    default: 
       plasma(random(80,400));
  }
}

void loop() {
  randomAnimation();
//  int yippieyeah[]={24,8,15,15,8,4,24,4,0,7};
//  printCodedWord(yippieyeah,10);
  
}















